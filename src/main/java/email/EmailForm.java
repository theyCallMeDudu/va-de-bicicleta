package email;

public class EmailForm 
{
	public String email;
	public String mensagem;
	
	public EmailForm() 
	{
	
	}
	
	public EmailForm(String email, String mensagem) 
	{
		this.email = email;
		this.mensagem = mensagem;
	}
		
	public Email desserializar() 
	{
		return new Email(this.email, this.mensagem);
	}
}
