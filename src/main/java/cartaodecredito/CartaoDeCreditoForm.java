package cartaodecredito;

public class CartaoDeCreditoForm 
{
	public String nomeTitular;
	public String numero;
	public String validade;
	public String cvv;
	
	public CartaoDeCreditoForm() 
	{
		
	} 
	
	public CartaoDeCreditoForm(String nomeTitular, String numero, String validade, String cvv)
	{
		this.nomeTitular = nomeTitular;
		this.numero = numero;
		this.validade = validade;
		this.cvv = cvv;
	}

	public CartaoDeCredito desserialize()
	{
		return new CartaoDeCredito(this.nomeTitular, this.numero, this.validade, this.cvv);
	}
}
