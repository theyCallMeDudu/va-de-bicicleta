package cobranca;

import io.javalin.http.Context;

public class FilaCobrancaController 
{
	
	private FilaCobrancaController() 
	{
		
	}
	
	public static void postFilaCobranca (Context ctx)
	{
    	
		try {
			CobrancaForm cobrancaJson = ctx.bodyAsClass(CobrancaForm.class);
			
	    	Cobranca cobranca = cobrancaJson.desserializar();
	    	
			FilaCobrancaDAO filaCobrancaDAO = new FilaCobrancaDAO();
			
	    	FilaCobranca fila = filaCobrancaDAO.getFilaCobrancaPreenchida();
	    	
	    	fila.setFilaCobranca(cobranca);
	    	
	    	ctx.status(200);
	    	ctx.json(fila);
		} catch(Exception ex) {
			ctx.status(422);
		}
		
    }
    

  	public void removeFilaCobranca() 
  	{
  		FilaCobrancaDAO filaCobrancaDAO = new FilaCobrancaDAO();
  		
  		FilaCobranca fila = filaCobrancaDAO.getFilaCobrancaPreenchida();
  		
  		Cobranca cobrancaRemovida = fila.getFilaCobranca().remove();
  		
  		ExpedicaoCobranca cobranca = new ExpedicaoCobranca();
  		
  		cobranca.setCobranca(cobrancaRemovida);
  		cobranca.enviaCobranca();  		
  	}
}